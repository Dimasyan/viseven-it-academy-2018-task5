import Vue from 'vue'
import Vuex from 'vuex'

import myProducts from './../products.json'
import myCategories from './../category.json'

Vue.use(Vuex)

export const store = new Vuex.Store({
	state: {
    products: myProducts,
    categories: myCategories,
    productsInCart: [],
  },

  getters: {
    getProductsInCart: (state) => {
      return state.productsInCart.map(i => ({
          ...i,
          product: state.products.find(j => j.id === i.id)
      }));
    },

    getAmounById: state => id => {
      return  state.productsInCart.find(i => i.id === id) === undefined
      ? 0
      : state.productsInCart.find(i => i.id === id).amount
    },

    getTotalSum: (state, getters) => {
      return getters.getProductsInCart.reduce((sum, product) => sum + product.amount * product.product.price, 0);
    }
  },

	mutations: {
    addToCart(state, id, amount = 1) {
      var p = state.productsInCart.find(i => i.id === id);
      if (p) {
        p.amount += amount;
      } else {
        state.productsInCart.push({id: id, amount: amount})
      }
      store.dispatch('setCartInLocalStorage');
    },

    subFromCart(state, id) {
      var p = state.productsInCart.find(i => i.id === id);
      if (p.amount > 0) {
        p.amount = p.amount - 1;
      } else {
        p.amount = 0;
      }
      store.dispatch('setCartInLocalStorage');
    },

    removeItemFromCart(state,id) {
      var i = state.productsInCart.findIndex(i => i.id === id);
      state.productsInCart.splice(i, 1);
      store.dispatch('setCartInLocalStorage');
    },

    clearCart(state) {
      state.productsInCart = [],
      store.dispatch('deletCartFromLocalStorage');
    }

  },

  actions: {
    getCartFromLocalStorage () {
      if (localStorage.getItem('__cart__')) {
        try {
          this.state.productsInCart = JSON.parse(localStorage.getItem('__cart__'));
        } catch(e) {
          localStorage.removeItem('__cart__');
        }
      }
    },

    setCartInLocalStorage () {
      localStorage.setItem('__cart__', JSON.stringify(this.state.productsInCart));
    },

    deletCartFromLocalStorage () {
      localStorage.removeItem('__cart__')
    },

  },
});
