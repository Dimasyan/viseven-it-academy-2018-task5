const category = [
  {
    id: 'c1',
    title: 'T-Shirts',
    image: 'sirts.png',
  },
  {
    id: 'c2',
    title: 'Cards',
    image: 'cards.png',
  },
  {
    id: 'c3',
    title: 'Pens & Pensils',
    image: 'pens.png',
  },
  {
    id: 'c4',
    title: 'Notebooks',
    image: 'notebooks.png',
  },
  {
    id: 'c5',
    title: 'Toys',
    image: 'toys.png',
  },
  {
    id: 'c6',
    title: 'Bags',
    image: 'bags.png',
  },
  {
    id: 'c7',
    title: 'scrum-cards',
    image: 'scrum_cards.png',
  },
  {
    id: 'c8',
    title: 'Magnets',
    image: 'magnets.png',
  },
  {
    id: 'c9',
    title: 'Cups',
    image: 'cups.png',
  },
  {
    id: 'c10',
    title: 'Magnets2',
    image: 'magnets.png',
  },
  {
    id: 'c11',
    title: 'Pens & Pensils2',
    image: 'pens.png',
  },
  {
    id: 'c12',
    title: 'Toys2',
    image: 'toys.png',
  },
];

var jsonData = JSON.stringify(category);

var fs = require('fs');
fs.writeFile("category.json", jsonData, function(err) {
  if (err) {
      console.log(err);
  }
});
