import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import App from './App.vue'

import { store } from './store/store'

import HomeView from './views/HomeView.vue'
import CategoryView from './views/CategoryView.vue'
import Cart from './views/CartView.vue'
import PageNotFoundView from './views/PageNotFoundView.vue'

Vue.config.productionTip = false

Vue.use(VueRouter);
Vue.use(Vuex);

const router = new VueRouter({
  routes: [
    { path: '/',
      component: HomeView },
    { path: '/category/:categoryTitle/:categoryId',
      component: CategoryView, props: true },
      { path: '/cart/',
      component: Cart, props: true },

    { path: '*',
      component: PageNotFoundView }
  ]
});

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')

